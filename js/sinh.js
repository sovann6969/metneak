
function img_find() {
  var imgs = document.getElementsByTagName("img");
  var imgSrcs = [];

  for (var i = 0; i < imgs.length; i++) {
    imgSrcs.push(imgs[i].src);
  }

  return imgSrcs;
}
function onClickLike(icon) {
  icon = $(icon);
  if (icon.hasClass('far'))
    icon.removeClass('far').addClass('fas text-danger');
  else
    icon.removeClass('fas text-danger').addClass('far');
}

function onClickComment(id) {
  $('#comment' + id).focus();
}

function replyComment(id, input) {
  if (input.value == '') return;

  let reply = `
    <div class="col-1"></div>
    <div class="col-11">
        <div>
        <a href="myProfile.html">
            <img class="profile" src="`+ img_find()[2] + `"
              class="rounded-circle z-depth-0 profile-picture float-left mr-3" alt="avatar image" height="35">
          </a>
            <h6 class="my-0">Justin Bieber</h6>
            <span class="grey-text small position-relative" style="top: -5px;"><span class="minute">1</span> minutes ago</span>
        </div>
        <p class="small mb-0">`+ input.value + `</p>
    </div>
  `;

  let replySection = $('#reply' + id);
  let replyComment = $('<div class="row mt-2"></div>');
  let hr = $('<hr class="my-1">');
  replyComment.html(reply);
  if (replySection.html() != '')
    replySection.append(hr);
  replySection.append(replyComment);
  input.value = '';

}

function editComment(id) {
  let userComment = $('#userComment' + id);
  let editComment = $('#edit_comment');
  $('#editCommentId').val(id);
  editComment.val(userComment.html());
  setTimeout(function () {
    editComment.focus();
  }, 200);

  

  let imgs = $('#img-comment').find('img');
  if (imgs.length > 0) {
    document.getElementById('edit_post_img').disabled = true;
    let editImgPreview = $('#edit_img_preview');
    let img = $('<img src="" height="30" width="30" id="edit_img_post">');
    let src = imgs[0].src;
    img.attr('src', src);

    let icon = `<a><i class="far fa-times-circle float-right text-danger" onclick="removeImg()" id="edit_img_selected"></i></a>`;

    editImgPreview.append(img).append(icon);
    
  }
}

//select image from local
let editPostImg = document.getElementById('edit_post_img');
editPostImg.onchange = function (evt) {
  evt.preventDefault();
  let imgs = $('#edit_img_preview').find('img');
  if (imgs.length > 0) return;

  postMsg.innerHTML = ""
  var tgt = evt.target || window.event.srcElement,
      files = tgt.files;

  // FileReader support
  if (FileReader && files && files.length) {
    var fr = new FileReader();
    fr.onload = function () {

      //only one image 

      let editImgPreview = $('#edit_img_preview');
      let img = $('<img src="" height="30" width="30" id="edit_img_post">');
      img.attr('src', fr.result);
  
      let icon = `<a><i class="far fa-times-circle float-right text-danger" onclick="removeImg()" id="edit_img_selected"></i></a>`;
  
      editImgPreview.append(img).append(icon);

    
    }
    fr.readAsDataURL(files[0]);
  }
  // Not supported
  else {
      check = false;

  }
}

function removeImg() {
  document.getElementById('edit_post_img').disabled = false;
  document.getElementById('edit_postMsg').innerHTML = ""
  $("#edit_img_post").remove();
  $("#edit_img_selected").remove();
  console.log('edit_img_selected');
};

function deleteComment(id) {

  let commentSection = $('#commentSection' + id);
  if (confirm("Are u sure ?"))
    commentSection.remove();
}

$('#btn_submit_edit').click(function () {
  let editComment = $('#edit_comment');
  let editId = $('#editCommentId').val();
  let comment = $('#userComment'+editId);
  let editImg = $('#edit_img_post');
  let src = editImg.attr('src');

  comment.html(editComment.val());

  if (editImg.length == 0) {
    $('#imgComment'+editId).remove();
    return;
  }

  
  let imgComment = $('#imgComment'+editId);
  if (imgComment.length > 0) {
    imgComment.attr('src', src);
  } else {
    $('#userComment'+editId).next().append($('<img id="imgComment'+editId+'" src="'+src+'" style="width: 100%">')); 
  }


  editImg.next().remove();
  editImg.remove();
});

