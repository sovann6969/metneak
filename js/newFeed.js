var text_area = document.getElementById("text_area");
var btn_close = document.getElementById("btn_win_close");
var check = false;
var img_select = document.getElementById("img_selected");
let commentId = 1;

$(document).ready(function () {

    
    //select image from local
    let postImg = document.getElementById('post_img');
    postImg.onchange = function (evt) {
        evt.preventDefault();
        let imgs = $('#img_preview').find('img');
        if (imgs.length > 0) return;

        postMsg.innerHTML = ""
        var tgt = evt.target || window.event.srcElement,
            files = tgt.files;

        // FileReader support
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {

                //only one image 
                var imgSrc = document.getElementById("img_selected")
                if (imgSrc == null) {
                    // document.getElementById('img_post').src = fr.result;
                    var ea = document.createElement("a")
                    var ei = document.createElement("i")
                    var img = document.createElement("img")

                    img.src = fr.result;
                    img.height = 30;
                    img.width = 30;
                    img.setAttribute('id', 'img_post');

                    ei.className = 'far fa-times-circle float-right text-danger';
                    ei.setAttribute('id', 'img_selected');
                    ea.appendChild(ei)

                    $(ei).click(clearImagePost);
                    document.getElementById('img_preview').appendChild(img)
                    document.getElementById('img_preview').appendChild(ea)
                    check = true;
                }
            }
            fr.readAsDataURL(files[0]);
        }
        // Not supported
        else {
            check = false;

        }
    }

    function img_find() {
        var imgs = document.getElementsByTagName("img");
        var imgSrcs = [];

        for (var i = 0; i < imgs.length; i++) {
            imgSrcs.push(imgs[i].src);
        }

        return imgSrcs;
    }
    //submit result
    $('#btn_submit').click(function (event) {
        event.preventDefault();
        // console.log(text_area.value)
        if (validatePost(text_area) == true || check) {
            // var imgs = document.getElementById("avatar_profile").src;

            let comment = `
                <div class="card mb-0">
                
                            
                    <div class="view overlay">
                    </div>
                
                    <div class="card-body">

                        <div class="mb-2">
                        <a href="myProfile.html">
                            <img src="` + img_find()[2] + `"
                      
                            class="profile rounded-circle z-depth-0 profile-picture float-left mr-3" alt="avatar image" height="50">
                        </a>
                            <h5 class="my-0 mb-1">
                               Justin Bierber
                                
                                <div class="fa-pull-right">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <lable class="mr-4" id="user-name"></lable>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-lg-right dropdown-primary"
                                        aria-labelledby="navbarDropdownMenuLink-55">
                                        <a class="dropdown-item px-5" href="javascript:void(0)" data-toggle="modal" data-target="#modalCart" onclick="editComment(` + commentId + `)"><i class="fas fa-pen"></i> &nbsp; កែប្រែ</a>

                                        <a class="dropdown-item px-5" href="javascript:void(0)" onclick="deleteComment(` + commentId + `)"><i class="fas fa-trash-alt"></i> &nbsp; លុប</a>
                                    </div>
                                </div>
                            </h5>
                            <span class="grey-text"><span class="minute">1</span> minutes ago</span>
                        </div>

                        <p class="card-text mb-2" id="userComment` + commentId + `">` + text_area.value + `</p>
                        <div id="img-comment"></div>
                        
                        <hr class="my-1">

                        <div>
                            <i class="far fa-heart fa-2x" onclick="onClickLike(this)"></i>

                            <i class="far fa-comment fa-2x fa-pull-right" onclick="onClickComment(` + commentId + `)"></i>
                        </div>

                        <hr class="my-1">

                        <div id="reply` + commentId + `"></div>
                        

                        <input id="comment` + commentId + `" class="form-control form-control-sm mt-2" type="text" placeholder="បញ្ចេញមតិរបស់អ្នក ...." onchange="replyComment(` + commentId + `, this)">
                
                    </div>
            
                </div>`;



            let commentSection = $('#commentSection');
            let div = $('<div class="card-deck mt-2" id="commentSection' + commentId + '"></div>');
            div.html(comment);

            commentSection.prepend(div);
            text_area.value = "";
            $('.dropdown-toggle').dropdown('toggle')

            if (check) {
                let imgPost = $('#img_post');
                let src = imgPost.attr('src');
                let img = $('<img src="'+src+'" style="width: 100%" id="imgComment'+commentId+'">');
                $('#img-comment').append(img);
                imgPost.next().remove();
                imgPost.remove();
                check = false;
                postImg.disabled = false;
            }
            commentId++;
        } else {
            postMsg.className = "red-text"
            postMsg.innerHTML = "<i class='fas fa-exclamation-triangle'></i> &nbsp;&nbsp;&nbsp;" + "សូមបំពេញអោយបានត្រឹមត្រូវ" + " &nbsp;&nbsp;&nbsp;"
            var ea = document.createElement("a")
            var ei = document.createElement("i")
            ei.className = 'far fa-times-circle float-right text-danger';
            ei.setAttribute('id', 'img_selected_error')
            $(ei).click(clearPost);
            ea.appendChild(ei)
            postMsg.appendChild(ea)
        }
    });




    //press text area
    $('#text_area').keypress(function () {
        postMsg.innerHTML = ""
    })

    //click on msg text area
    $('#win_close').click(function () {
        postMsg.innerHTML = ""
    })

    //post  image
    $('#post_img').change(function () {
        // previewFile();
    })


    function clearPost() {
        postMsg.innerHTML = ""
        document.getElementById("img_selected_error").remove();
    }

    function clearImagePost() {
        check = false;
        postImg.disabled = false;
        postMsg.innerHTML = ""
        swallClearPost();
    }

    function swallClearPost(){
        Swal.fire({
            title: 'តើអ្នកពិតជាចង់បោះបង់ការបង្ហោះរូបភាពនេះមែនរឺ?',
            text: "រូបភាពនឹងមិនត្រូវបានបង្ហោះលើបណ្តាញសង្គមយើងខ្ញុំនោះឡើយ!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: "បោះបង់ជាឯកច្ឆន្ទ",
            confirmButtonText: 'ទទួលតាមសំណើរ'
          }).then((result) => {
            if (result.value) {
                document.getElementById("img_selected").remove();
                document.getElementById("img_post").remove();
              Swal.fire(
                'មិនបានបង្ហោះឡើយ!',
                'រូបភាពមិនត្រូវបានបង្ហោះនៅលើបណ្តាញយើងទេ',
                'success'
              )
            }
          })
    }


    //validate text area
    function validatePost(text) {
        if (text.value == "") {
            return false;
        } else if (text.value == undefined) {
            return false;
        } else {
            return true;
        }
    }


    setInterval(function () {
        let min = $('.minute');
        for (let i = 0; i < min.length; i++) {
            min[i].innerHTML = parseInt(min[i].innerHTML) + 1;
        }
    }, 60000);
})