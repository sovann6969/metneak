$(document).ready(function () {
    $('.mdb-select').materialSelect();
});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate() {
    var $result = $("#result");
    var email = $("#email").val();
    $result.text("");
    if (!validateEmail(email)) {
        $result.append('<i class="fas fa-exclamation-triangle"></i>'+" Couldn't find your email address");
        $result.css("color", "red");
    }
    return false;
}
